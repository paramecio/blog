from paramecio2.libraries.i18n import I18n, load_lang
from paramecio2.libraries.config_admin import config_admin

#from modules.pokermind.i18n import runchained

#modules_other=[I18n.lang('pages', 'pages', 'Pages'), 'modules.pages.admin.pages', 'pages']

#modules_admin.append(modules_other)


config_admin.append([I18n.lang('blog', 'blog', 'Blog')])

config_admin.append([I18n.lang('blog', 'posts', 'Posts'), 'modules.blog.admin.blog', 'admin_app.admin_blog'])
