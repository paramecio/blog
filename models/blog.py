from paramecio2.libraries.db.extrafields.i18nfield import I18nHTMLField, I18nField
from paramecio2.libraries.db.extrafields.slugifyfield import SlugifyField
from paramecio2.libraries.db.extrafields.datetimefield import DateTimeField
from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.db.extraforms.texthtmlform import TextHTMLForm
from paramecio2.libraries.db import corefields
from paramecio2.libraries.i18n import I18n
import json

class Post(WebModel):

    def create_fields(self):
        
        self.register(I18nField('title'), True)
        self.register(I18nHTMLField('lead', TextHTMLForm('lead', '')), True)
        self.register(I18nHTMLField('text', TextHTMLForm('text', '')), True)
        self.register(SlugifyField('slugify'), True)
        self.register(DateTimeField('date'), True)

    
