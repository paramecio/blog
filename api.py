from modules.blog import blog_app
from modules.blog.models.blog import Post
from flask import request, g, Response
from settings import config
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.libraries.i18n import I18n
from paramecio2.libraries.plugins import db
try:
    import ujson as json
except:
    import json
    
env=env_theme(__file__)

t=PTemplate(env)

@blog_app.route('/blog/get_last_posts')
@db
def get_last_posts():
    
    post=Post(g.connection)
    
    try:
        begin_post=request.args.get('begin_post', 0)
        
    except:
        begin_post=0
    
    q=post.query('select * from post order by date DESC limit %s, 10', [begin_post])

    arr_post=[]

    for row in q:
        row['date']=str(row['date'])
        arr_post.append(row)
    
    resp=Response(response=json.dumps(arr_post), status=200, mimetype="application/json")
    
    return resp

@blog_app.route('/blog/get_post/<post_id>')
@db
def get_post(post_id):
    
    post=Post(g.connection)
    
    q=post.query('select * from post WHERE id=%s', [post_id])
    
    arr_post=q.fetchone()
    
    arr_post['date']=str(arr_post['date'])
    
    return arr_post
    
    
